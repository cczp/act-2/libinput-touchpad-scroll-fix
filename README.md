# THIS PIECE OF SOFTWARE IS DEPRECATED

Consider using the option `scroll-factor` of [libinput-config].

[libinput-config]: https://gitlab.com/kirbykevinson/libinput-config

---

This workaround allows you to fix your laptop's touchpad if it's
affected by [this bug], i.e. if its sensitivity is abnormally high
when scrolling.

[this bug]: https://gitlab.gnome.org/GNOME/gtk/-/issues/1308 "two-finger scrolling is far too fast"

## How to use

1. Change the `LIBINPUT_SCROLL_FACTOR` variable in `main.c` to reflect
   how much your touchpad is broken (e.g. if it scrolls 10 times as
   fast as it should, set it to 0.1).
2. Compile the workaround and install it.

```
meson build
cd build
meson compile
sudo meson install
```

3. If you're using GNOME Shell, you might be affected by [this Fedora
   bug]. To work around it remove the capabilities from the shell's
   executable file.

[this Fedora bug]: https://ask.fedoraproject.org/t/ld-library-path-is-not-set-by-bash-profile-anymore-after-upgrade-to-fedora-31/4247 "LD_LIBRARY_PATH is not set by .bash_profile anymore after upgrade to Fedora 31"

```
sudo setcap "" /usr/bin/gnome-shell
```

## How it works

Most of Wayland compositors provide no way to configure the scrolling
speed, however they use the same function the get scrolling vectors
from libinput - `libinput_event_pointer_get_axis_value()` (more info
on it [here]).

[here]: https://wayland.freedesktop.org/libinput/doc/latest/api/group__event__pointer.html#ga81ad7d8a95c456731a874e584c4c8dda "libinput: Pointer events"

This workaround wraps around libinput and replaces the function with a
custom one that modifies the output value by multiplying it by a set
factor. To do this, it uses the `LD_PRELOAD` environment variable,
which is modified by a `/etc/profile.d` script.

## Inspiration

* [This comment by Spencer Russell](https://gitlab.gnome.org/GNOME/gtk/-/issues/1308#note_336954)
* [libinput-force-middle-click-emulation](https://github.com/gaul/libinput-force-middle-click-emulation)
